let olts =
{
    "content":
    [
        {
            "id": 3,
            "description": "GRT_OLT_1",
            "sysName": "GRT_OLT_1",
            "model": "G4S",
            "ipAddress": "172.30.121.1",
            "serialNumber": "MJ2RRWG136A0034"
        },
        {
            "id": 4,
            "description": "GRT_OLT_2",
            "sysName": "GRT_OLT_2",
            "model": "G4S",
            "ipAddress": "172.30.121.25",
            "serialNumber": "MJ2RRWG136A0035"
        }
    ],
    "totalElements": 2,
    "last": true,
    "totalPages": 1,
    "sort": null,
    "numberOfElements": 2,
    "first": true,
    "size": 2,
    "number": 0
};

module.exports = olts;