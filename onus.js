let onus_olt_1 =
[
    {
        "distance": 0,
        "id": 1,
        "index": 0,
        "iphostAddress": "string",
        "lastActive": true,
        "lastActiveAt": "2020-07-30T12:52:21.936Z",
        "olt": {
            "description": "GRT_OLT_1",
            "host": "172.30.121.1",
            "id": 3
        },
        "onu": {
            "description": "ONU1",
            "firmwareVersion": "onu_fw_version",
            "id": 1,
            "macAddress": "onu_mac_address",
            "model": "onu_model",
            "serialNumber": "FIOG54009524"
        },
        "operationalStatus": "string",
        "planNameInOltDevice": "string",
        "planStatus": "string",
        "port": 0,
        "rxPower": 0,
        "slot": 0,
        "updatedAt": "2020-07-30T12:52:21.936Z"
    },
    {
        "distance": 0,
        "id": 2,
        "index": 0,
        "iphostAddress": "string",
        "lastActive": true,
        "lastActiveAt": "2020-07-30T12:52:21.936Z",
        "olt": {
            "description": "GRT_OLT_1",
            "host": "172.30.121.1",
            "id": 3
        },
        "onu": {
            "description": "ONU2",
            "firmwareVersion": "onu_fw_version",
            "id": 2,
            "macAddress": "onu_mac_address",
            "model": "onu_model",
            "serialNumber": "FIOG54009525"
        },
        "operationalStatus": "string",
        "planNameInOltDevice": "string",
        "planStatus": "string",
        "port": 0,
        "rxPower": 0,
        "slot": 0,
        "updatedAt": "2020-07-30T12:52:21.936Z"
    },
    {
        "distance": 0,
        "id": 3,
        "index": 0,
        "iphostAddress": "string",
        "lastActive": true,
        "lastActiveAt": "2020-07-30T12:52:21.936Z",
        "olt": {
            "description": "GRT_OLT_1",
            "host": "172.30.121.1",
            "id": 3
        },
        "onu": {
            "description": "ONU3",
            "firmwareVersion": "onu_fw_version",
            "id": 1,
            "macAddress": "onu_mac_address",
            "model": "onu_model",
            "serialNumber": "FIOG54009526"
        },
        "operationalStatus": "string",
        "planNameInOltDevice": "string",
        "planStatus": "string",
        "port": 0,
        "rxPower": 0,
        "slot": 0,
        "updatedAt": "2020-07-30T12:52:21.936Z"
    }
];

let onus_olt_2 = 
[
    {
        "distance": 0,
        "id": 4,
        "index": 0,
        "iphostAddress": "string",
        "lastActive": true,
        "lastActiveAt": "2020-07-30T12:52:21.936Z",
        "olt": {
            "description": "GRT_OLT_2",
            "host": "172.30.121.25",
            "id": 4
        },
        "onu": {
            "description": "ONU4",
            "firmwareVersion": "onu_fw_version",
            "id": 1,
            "macAddress": "onu_mac_address",
            "model": "onu_model",
            "serialNumber": "FIOG54009527"
        },
        "operationalStatus": "string",
        "planNameInOltDevice": "string",
        "planStatus": "string",
        "port": 0,
        "rxPower": 0,
        "slot": 0,
        "updatedAt": "2020-07-30T12:52:21.936Z"
    },
    {
        "distance": 0,
        "id": 5,
        "index": 0,
        "iphostAddress": "string",
        "lastActive": true,
        "lastActiveAt": "2020-07-30T12:52:21.936Z",
        "olt": {
            "description": "GRT_OLT_2",
            "host": "172.30.121.25",
            "id": 4
        },
        "onu": {
            "description": "ONU5",
            "firmwareVersion": "onu_fw_version",
            "id": 2,
            "macAddress": "onu_mac_address",
            "model": "onu_model",
            "serialNumber": "FIOG54009528"
        },
        "operationalStatus": "string",
        "planNameInOltDevice": "string",
        "planStatus": "string",
        "port": 0,
        "rxPower": 0,
        "slot": 0,
        "updatedAt": "2020-07-30T12:52:21.936Z"
    }
];

let onus = {
    "content": [],
    "first": true,
    "last": true,
    "number": 0,
    "numberOfElements": 5,
    "size": 0,
    "sort": {},
    "totalElements": 0,
    "totalPages": 0
}

module.exports = { onus, onus_olt_1, onus_olt_2 };