# **DISPOSITIVOS**


**OLTs:**
- GRT_OLT_1 (172.30.121.1)
- GRT_OLT_2 (172.30.121.25)

**ONUs:**
- GRT_OLT_1
	- ONU1 (FIOG54009524)
	- ONU2 (FIOG54009525)
	- ONU3 (FIOG54009526)

- GRT_OLT_2
	- ONU4 (FIOG54009527)
	- ONU5 (FIOG54009528)

# **ENDPOINTS**

	curl -X GET localhost:8765/api/ispmanager/olt

Retorna todas as OLTs

	curl -X GET localhost:8765/api/ispmanager/onu

Retorna todas as ONUs

	curl -X GET localhost:8765/api/ispmanager/onu?oltIp=172.30.121.1

Retorna as ONUs 1, 2 e 3.

	curl -X GET localhost:8765/api/ispmanager/onu?oltIp=172.30.121.25

Retorna as ONUs 4 e 5.

	curl -X GET localhost:8765/api/ispmanager/onu?oltIp=172.30.121.8

Retorna um array vazio do ONUs

# **EXECUÇÃO**
	npm install
	node isp.js