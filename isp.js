const app = require('./express')();
const olts = require('./olts');
const onus_mock_data = require('./onus');
const port = app.get('port');

app.listen(port, () => {
	console.log(`Server running on port ${port}`)
});

app.get('/api/ispmanager/olt', function (req, res) { res.json(olts); })

app.get('/api/ispmanager/onu', function (req, res) {

	let oltIp = req.query.oltIp;

	if (!req.query.oltIp) {
		onus_mock_data.onus.content = onus_mock_data.onus_olt_1.concat(onus_mock_data.onus_olt_2);
	} else if (req.query.oltIp === "172.30.121.1") {
		onus_mock_data.onus.content = onus_mock_data.onus_olt_1;
	} else if (req.query.oltIp === "172.30.121.25") {
		onus_mock_data.onus.content = onus_mock_data.onus_olt_2;
	} else {
		onus_mock_data.onus.content = [];
	}

	onus_mock_data.onus.size = onus_mock_data.onus.content.length;
	onus_mock_data.onus.totalElements = onus_mock_data.onus.content.length;
	onus_mock_data.onus.numberOfElements = onus_mock_data.onus.content.length;

	res.json(onus_mock_data.onus);
})
